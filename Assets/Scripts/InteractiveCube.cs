﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractiveCube : MonoBehaviour
{
    public GameColor gameColor;
    public Rigidbody rigidody;

    public float leftBound;
    public float rightBound;
    public float uperBound;
    public float lowerBound;

    public Vector3 startPos;
    void Start()
    {
        rigidody = this.GetComponent<Rigidbody>();
    }

    internal void Release()
    {
        if (CubeController.heldCube == this)
            CubeController.heldCube = null;

        rigidody.velocity = Vector3.ClampMagnitude(rigidody.velocity, 15f);
    }
    // Наверное, на телефоне проверка на выход за пределы будет избыточна
    private void Update()
    {
        if (this.transform.position.x > rightBound
            || this.transform.position.x < leftBound
            || this.transform.position.y > uperBound
            || this.transform.position.y < lowerBound)
        {
            ResetCubePosition();
        }
    }
    private void ResetCubePosition()
    {
        Release();
        rigidody.velocity = Vector3.zero;
        this.transform.position = startPos;
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.collider.TryGetComponent(out Wall wall))
        {
            if(wall.gameColor == this.gameColor)
            {
                GameManager.ChangeScore(this.gameColor, 1);

                if(CubeController.heldCube == this)
                    CubeController.heldCube = null;

                Destroy(this.gameObject);
            }
            else
            {
                rigidody.velocity = new Vector3(-rigidody.velocity.x + ((rigidody.velocity.x < 0) ? 1f : -1f), rigidody.velocity.y, 0);
                Release();
            }
        }
    }
}
