﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
public class QuadSetting : MonoBehaviour
{
    Vector3 etalonRatio = new Vector3(9, 16, 1);
    public Vector2 resolution;
    float screenRatio;
    // Start is called before the first frame update
    public void SetSize()
    {
        resolution = new Vector2 (Screen.currentResolution.width, Screen.currentResolution.height);
        screenRatio = (float)Screen.currentResolution.height / (float)Screen.currentResolution.width;
        this.transform.localScale = new Vector3(etalonRatio.y / screenRatio, this.transform.localScale.y, 1);
    }

}
