﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Score : ItemToinitialize
{
    public GameColor gameColor;
    TMP_Text scoreTextField;

    public override void Initialize()
    {
        scoreTextField = this.GetComponent<TMP_Text>();

        GameManager.updateScoreUI[gameColor].AddListener(OnScoreUpdated);
    }

    void OnScoreUpdated(int score)
    {
        scoreTextField.text = score.ToString();
    }

}
