﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using System.Linq;
using TMPro;
public class GameManager : MonoBehaviour
{
    public class UnityEventInt : UnityEvent<int> { }

    public GameObject gameOverScreen;
    [Space]
    public Transform interactiveCubesParent;
    [Space]
    // Для проверки на выход куба за пределы
    public Transform rightWall;
    public Transform leftWall;
    public Transform floor;
    public Transform ceiling;
    [Space]
    public List<ItemToinitialize> itemsToinitialize;
    [Space]
    public List<GameObject> cubePrefabs;

    public static Dictionary<GameColor, UnityEventInt> updateScoreUI;

    private static Dictionary<GameColor, int> maxScores;
    private static Dictionary<GameColor, int> scores;

    private List<Tuple<Vector3,GameColor>> cubesAtStart;

    private static UnityEvent gameOver;

    public static void ChangeScore(GameColor color, int d)
    {
        scores[color]+=d;
        updateScoreUI[color].Invoke(scores[color]);
        if(scores.Sum(t=>t.Value) == maxScores.Sum(t=>t.Value))
        {
            gameOver.Invoke();
        }
    }

    private void GameOver()
    {
        gameOverScreen.SetActive(true);
    }
    public void RestartGame()
    {
        foreach (var cubeData in cubesAtStart)
        {
            InteractiveCube cube =  Instantiate(cubePrefabs.Find(t => t.GetComponent<InteractiveCube>().gameColor == cubeData.Item2),
                cubeData.Item1, Quaternion.identity,interactiveCubesParent).GetComponent<InteractiveCube>();

            // Для проверки на выход куба за пределы
            cube.leftBound = leftWall.position.x;
            cube.rightBound = rightWall.position.x;
            cube.uperBound = ceiling.position.y;
            cube.lowerBound = floor.position.y;

            ChangeScore(cubeData.Item2, -1);
        }

    }

    
    void Start()
    {
        cubesAtStart = new List<Tuple<Vector3, GameColor>>();
        gameOver = new UnityEvent();
        gameOver.AddListener(GameOver);

        scores = new Dictionary<GameColor, int>();
        updateScoreUI = new Dictionary<GameColor, UnityEventInt>();
        maxScores = new Dictionary<GameColor, int>();

        scores.Add(GameColor.Blue, 0);
        updateScoreUI.Add(GameColor.Blue, new UnityEventInt());

        scores.Add(GameColor.Red, 0);
        updateScoreUI.Add(GameColor.Red, new UnityEventInt());

        itemsToinitialize.ForEach(t => t.Initialize());

        foreach (var item in updateScoreUI)
            item.Value.Invoke(0);

        InteractiveCube [] cubes = FindObjectsOfType<InteractiveCube>();

        FindObjectOfType<QuadSetting>().SetSize();

        foreach (var cube in cubes)
        {
            if (!maxScores.ContainsKey(cube.gameColor))
                maxScores.Add(cube.gameColor, 0);
            maxScores[cube.gameColor]++;

            cube.transform.position = new Vector3(cube.transform.position.x, cube.transform.position.y, GameObject.Find("InteractivePlane").transform.position.z);
            cube.startPos = cube.transform.position;

            cubesAtStart.Add(new Tuple<Vector3, GameColor>(cube.startPos, cube.gameColor));


            // Для проверки на выход куба за пределы
            cube.leftBound = leftWall.position.x;
            cube.rightBound = rightWall.position.x;
            cube.uperBound = ceiling.position.y;
            cube.lowerBound= floor.position.y;
        }

    }


}
