﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeController : MonoBehaviour
{
    public static InteractiveCube heldCube = null;
    private Vector3 holdingD;

    public float cubeFollowVelocity = 2f;

    void Update()
    {
        if (heldCube != null)
        {
            if (Input.GetMouseButtonUp(0))
            {
                heldCube.Release();
            }
            else
            {
                if (TryGetPlaneHit(out RaycastHit planeHit))
                {
                    Vector3 targetVelocity = (planeHit.point - heldCube.transform.position - holdingD) * cubeFollowVelocity;

                    heldCube.rigidody.velocity = targetVelocity * targetVelocity.magnitude;
                }
                else heldCube.Release();
            }
        }
        else
        {
            TryPickCube();
        }
    }

    private bool TryGetPlaneHit(out RaycastHit planeHit)
    {

#if UNITY_EDITOR || UNITY_STANDALONE_WIN
       return Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out planeHit, 100f, LayerMask.GetMask("TouchPlane"));
#endif

#if (UNITY_ANDROID || UNITY_IPHONE) && !UNITY_EDITOR
       return Physics.Raycast(Camera.main.ScreenPointToRay(Input.touches[0].position), out planeHit, 100f, LayerMask.GetMask("TouchPlane"));
#endif
    }

    private void TryPickCube()
    {
#if UNITY_EDITOR || UNITY_STANDALONE_WIN

        if (Input.GetMouseButtonDown(0))
        {
            if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out RaycastHit cubeHit, 100f, LayerMask.GetMask("Cube"))
                && cubeHit.collider.TryGetComponent(out InteractiveCube cube))
            {
                if (TryGetPlaneHit(out RaycastHit planehit))
                {
                    holdingD = planehit.point - cubeHit.collider.transform.position;
                    holdingD.z = 0;
                    heldCube = cube;
                }
            };
        }
#endif

#if (UNITY_ANDROID || UNITY_IPHONE) && !UNITY_EDITOR
       if (Input.touchCount > 0 && Input.touches[0].phase == TouchPhase.Began)
        {
            if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.touches[0].position), out RaycastHit cubeHit, 100f, LayerMask.GetMask("Cube"))
                && cubeHit.collider.TryGetComponent(out InteractiveCube cube))
            {
                if (TryGetPlaneHit(out RaycastHit planehit))
                {
                    holdingD = planehit.point - cubeHit.collider.transform.position;
                    holdingD.z = 0;
                    heldCube = cube;
                }
            }
        } 
#endif
    }
}
